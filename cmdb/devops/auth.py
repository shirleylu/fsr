# ~*~ coding: utf-8 ~*~

import importlib,sys 
importlib.reload(sys)
from django.shortcuts import render_to_response,HttpResponse,HttpResponseRedirect


def check(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/redir/')
