# _*_ coding: utf-8 _*_
__author__ = 'HaoChenXiao'
from django.conf.urls import url, include

from devops import views

urlpatterns = [
	url(r'^monitor_config_stop/$', views.MonitorConfigStopView.as_view(), name = 'monitor_config_stop'),
	url(r'^monitor_config_start/$', views.MonitorConfigStartView.as_view(), name = 'monitor_config_start'),
	url(r'^monitorconfig_edit/$', views.MonitorConfigEditView.as_view(), name = 'monitorconfig_edit'),
	url(r'^monitor_config_delete/$', views.MonitorConfigDeleteView.as_view(), name = 'monitor_config_delete'),
	url(r'^monitor_config/$', views.MonitorConfigView.as_view(), name = 'monitor_config'),
	url(r'^autorecovery/$', views.AutoRecoveryView.as_view(), name = 'auto_recovery'),
	url(r'^autorecovery_start/$', views.AutoRecoveryStartView.as_view(), name = 'auto_recovery_start'),
	url(r'^autorecovery_stop/$', views.AutoRecoveryStopView.as_view(), name = 'auto_recovery_stop'),
	url(r'^autorecovery_edit/$', views.AutoRecoveryEditView.as_view(), name = 'autorecovery_edit'),
	url(r'^auto_recovery_delete/$', views.AutoRecoveryDeleteView.as_view(), name = 'auto_recovery_delete'),
]